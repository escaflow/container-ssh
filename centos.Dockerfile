FROM quay.io/centos/centos:stream9

RUN dnf install epel-release epel-next-release -y \
    && dnf install -y \
      dropbear openssh-clients sudo unzip python3 python3-dnf python3-pip \
    && dnf clean all \
    && echo 'root:rootpassword' | /usr/sbin/chpasswd \
    && sudo useradd -rm -s /bin/bash -g root -G wheel -u 1000 ansible \
    && echo 'ansible:ansiblepassword' | /usr/sbin/chpasswd

EXPOSE 2222
CMD ["/usr/sbin/dropbear", "-E", "-F", "-R", "-p", "2222"]
