#!/bin/bash

CONTAINER_NAME="container-ssh"

# echo "building ${CONTAINER_NAME}/debian:latest"
# buildah bud -f debian.Dockerfile -t localhost/${CONTAINER_NAME}/debian:latest .

# echo "building ${CONTAINER_NAME}/ubuntu:latest"
# buildah bud -f ubuntu.Dockerfile -t localhost/${CONTAINER_NAME}/ubuntu:latest .

echo "building ${CONTAINER_NAME}/fedora:latest"
buildah bud -f fedora.Dockerfile -t localhost/${CONTAINER_NAME}/fedora:latest .

# echo "building ${CONTAINER_NAME}/centos:latest"
# buildah bud -f centos.Dockerfile -t localhost/${CONTAINER_NAME}/centos:latest .
