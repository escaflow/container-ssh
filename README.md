# Why?
I need a simple ssh images for testing purposes.
DON'T USE THEM FOR ANYTHING ELSE!

# Usage
Start up your flavor: 
```
podman run -p 2222:2222 registry.gitlab.com/escaflow/container-ssh/debian:latest
```

You want to disable a few things, namely StrictHostKeyChecking, PubkeyAuthentication and UserKnownHostsFile so the login may look like this:
```
ssh ansible@127.0.0.1 -p 2222 -o StrictHostKeyChecking=no  -o PubkeyAuthentication=no -o UserKnownHostsFile=/dev/null
```
Alternatively you can put this in your ~/.ssh/config:
```
Host 127.0.0.1
  Port 2222
  StrictHostKeyChecking no
  PubkeyAuthentication no
  UserKnownHostsFile /dev/null
```
