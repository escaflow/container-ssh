FROM docker.io/ubuntu:latest
ARG DEBIAN_FRONTEND=noninteractive

RUN apt-get update && apt-get install -y --no-install-recommends \
      dropbear openssh-client sudo unzip python3 python3-apt python3-pip \
    && apt-get clean && rm -f /var/lib/apt/lists/*_* \
    && echo 'root:rootpassword' | chpasswd \
    && userdel ubuntu \
    && useradd -rm -s /bin/bash -g root -G sudo -u 1000 ansible \
    && echo 'ansible:ansiblepassword' | chpasswd

EXPOSE 2222
CMD ["/usr/sbin/dropbear", "-E", "-F", "-R", "-p", "2222"]
